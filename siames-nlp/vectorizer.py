import glob
import logging
import shutil
import uuid
from os import path

import numpy
import numpy as np
import pandas as pd
import spacy
from owlready2 import *
from spacy.tokens import Doc
from spacy.vectors import Vectors

FILE_NAME = 'data/siames.xml'
OUTPUT = 'out2/vectors'
VECTOR_FOLDER_PREFIX = 'vec_folder_'
VECTOR_DIM = 300
TEXTS_CSV = 'features_texts.csv'
DIMENSIONS_CSV = 'dimensions.csv'

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', level='INFO')


def compare():
    directory = OUTPUT
    results = pd.DataFrame(
        columns=['case1', 'case2', 'feature', 'vector_key_1', 'vector_text1', 'vector_key_2', 'vector_text2', 'score'])
    texts = pd.read_csv(path.join(directory, TEXTS_CSV), index_col='index')
    dimensions = pd.read_csv(path.join(directory, DIMENSIONS_CSV), index_col='index')
    logging.info('{} features read'.format(texts.size))

    vectors_frame = retrieve_vectors_frame(directory)
    vectors_grouped = group_vectors(vectors_frame)

    for case, _ in vectors_frame.groupby(['case']):
        case_features = vectors_frame.loc[vectors_frame['case'] == case, ['feature', 'vector_key', 'vector']]
        for _, [feature, vector_key, vector] in case_features.iterrows():
            other_vectors = vectors_grouped.loc[
                (vectors_grouped['case'] != case) & (vectors_grouped['feature'] == feature), ['case', 'vectors']]
            for _, vectors in other_vectors.iterrows():
                if dimensions.loc[case]['dimension'] == dimensions.loc[vectors['case']]['dimension']:
                    similarities = vectors['vectors'].most_similar(numpy.asarray([vector]))
                    keys, best_rows, scores = similarities
                    results = results.append(
                        {'case1': case,
                         'case2': vectors['case'],
                         'feature': feature,
                         'vector_key_1': vector_key,
                         'vector_text1': texts.loc[vector_key].values[0],
                         'vector_key_2': keys[0][0],
                         'vector_text2': texts.loc[keys[0][0]].values[0],
                         'score': scores[0][0]},
                        ignore_index=True
                    )
    save_results(directory, results)


def save_results(directory: str, results: pd.DataFrame):
    results.to_csv(path.join(directory, 'full_results.csv'))
    mean_by_feature = results.groupby(['case1', 'case2', 'feature'])['score'].mean().to_frame()
    mean_by_feature.to_csv(path.join(directory, 'mean_by_feature.csv'))
    final_results = mean_by_feature.groupby(['case1', 'case2'])['score'].mean().to_frame().sort_values(
        by=['case1', 'score'],
        ascending=[True, False])
    final_results.to_csv(path.join(directory, 'results.csv'))
    print(final_results)


def group_vectors(vectors_frame: pd.DataFrame):
    vectors_grouped = pd.DataFrame(columns=['feature', 'case', 'vectors'])
    for [case, feature], size in vectors_frame.groupby(['case', 'feature']).size().items():
        vectors = Vectors(shape=(size, VECTOR_DIM))
        [vectors.add(key=v['vector_key'], vector=v['vector'])
         for _, v in vectors_frame.loc[
             (vectors_frame['feature'] == feature) & (vectors_frame['case'] == case), ['vector_key',
                                                                                       'vector']].iterrows()]
        vectors_grouped = vectors_grouped.append({'feature': feature, 'case': case, 'vectors': vectors},
                                                 ignore_index=True)
    return vectors_grouped


def retrieve_vectors_frame(directory: str):
    vectors_path_pattern = '{}{}**{}{}*'.format(directory, os.sep, os.sep, VECTOR_FOLDER_PREFIX)
    vectors_frame = pd.DataFrame(columns=['feature', 'case', 'vector_key', 'vector'])
    for vector_folder in glob.glob(vectors_path_pattern, recursive=True):
        _, _, feature, case, _ = list(vector_folder.split(os.sep))
        vector = Vectors().from_disk(vector_folder)
        key, vector = next(vector for vector in vector.items())
        vectors_frame = vectors_frame.append({'feature': feature, 'case': case, 'vector_key': key, 'vector': vector},
                                             ignore_index=True)
    return vectors_frame


def run(file_name: str = FILE_NAME):
    logging.info('Start')

    logging.info('Loading ontology')
    sustainability_onto = get_ontology("file://data/sustainability.owl").load()
    onto = get_ontology('file://{}'.format(file_name)).load()

    logging.info('Loading spacy')
    nlp = spacy.load('en_vectors_web_lg', disable=["ner", "textcat"])
    nlp.add_pipe(preprocess)

    logging.info('Cleaning output folder')
    shutil.rmtree(OUTPUT, ignore_errors=True)

    logging.info('Generating and saving features vectors')
    features_texts = pd.DataFrame(columns=['text'])

    counter = 0
    for vector in get_vectors(onto, nlp):
        key = save_vector(vector['vector'], vector['case'], vector['feature'])
        features_texts.loc[key] = [vector['text']]
        counter += 1
    logging.info('{} features written'.format(counter))

    logging.info('Saving main dimensions')
    cases_dimensions = pd.DataFrame(columns=['dimension'])
    for case in onto.search(type=onto.Case):
        cases_dimensions.loc[case.name] = get_main_dimension(case)

    features_texts.to_csv(path.join(OUTPUT, TEXTS_CSV), index_label='index')
    cases_dimensions.to_csv(path.join(OUTPUT, DIMENSIONS_CSV), index_label='index')

    logging.info('Comparing')
    compare()

    logging.info('Finish')


def preprocess(doc):
    result = [token.text.lower() for token in doc if
              token.text not in spacy.lang.en.stop_words.STOP_WORDS and not token.is_punct and token.lemma_ != '-PRON-']
    return Doc(doc.vocab, words=result)


def save_vector(data: np.ndarray, case: str, feature: str):
    data = np.asarray([data])
    key = get_key()
    vectors = Vectors(data=data, keys=[key])
    output_path = path.join(OUTPUT, feature, case)
    os.makedirs(output_path, exist_ok=True)
    vectors.to_disk(path.join(output_path, '{}{}'.format(VECTOR_FOLDER_PREFIX, key)))
    return key


def get_key():
    return uuid.uuid1().int >> 64


def get_main_dimension(case):
    return case.main_dimension[0].name


def get_vectors(onto: owlready2.Ontology, nlp: spacy.language):
    for case in onto.search(type=onto.Case):
        yield from get_sectors(case, nlp)
        yield from get_outcomes_and_suboutcomes(case, nlp)
        yield from get_stakeholders(case, nlp)


def get_stakeholders(case, nlp):
    for indicator in case.has_indicator:
        for stakeholder in indicator.has_receiver:
            doc = nlp(stakeholder.has_literal[0])
            yield {'case': case.name, 'feature': 'Stakeholder', 'name': stakeholder.name,
                   'vector': doc.vector, 'text': doc.text}


def get_outcomes_and_suboutcomes(case, nlp):
    for outcome in case.has_outcome:
        doc = nlp(outcome.has_literal[0])
        yield {'case': case.name, 'feature': 'Outcome', 'name': outcome.name, 'vector': doc.vector, 'text': doc.text}

        for suboutcome in case.has_suboutcome:
            doc = nlp(suboutcome.has_literal[0])
            yield {'case': case.name, 'feature': 'Suboutcome', 'name': suboutcome.name, 'vector': doc.vector,
                   'text': doc.text}


def get_sectors(case, nlp):
    for sector in case.has_sector:
        doc = nlp(sector.has_literal[0])
        yield {'case': case.name, 'feature': 'Sector', 'name': sector.name, 'vector': doc.vector, 'text': doc.text}


if __name__ == '__main__':
    run()
